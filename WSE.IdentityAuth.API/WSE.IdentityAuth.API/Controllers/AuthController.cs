﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WSE.IdentityAuth.Interface;
using WSE.IdentityAuth.Model;
using WSE.IdentityAuth.Model.Request;

namespace WSE.IdentityAuth.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthVerification authRepository;

        public AuthController(IAuthVerification _authRepository)
        {
            authRepository = _authRepository;
        }

        [Route("bymobile")]
        public async Task<APIResult> MobileVerification(PhoneAuthRequest request)
        {

            var result = await authRepository.PhoneVerification(request);

            if (result)
            {
                return new APIResult()
                {
                    status = ResponseStatusResult.OK
                };
            }
            else
            {
                return new APIResult()
                {
                    status = ResponseStatusResult.BadRequest
                };
            }
        }

        [Route("bybankcard")]
        public async Task<APIResult> BankCardVerification(BankCardAuthRequest request)
        {
            var result = await authRepository.BankCardVerification(request);

            if (result)
            {
                return new APIResult()
                {
                    status = ResponseStatusResult.OK
                };
            }
            else
            {
                return new APIResult()
                {
                    status = ResponseStatusResult.BadRequest
                };
            }
        }

        [Route("byimage")]
        public async Task<APIResult> Verification(ImageAuthRequest request)
        {
            var result = await authRepository.ImageVerification(request);

            if (result)
            {
                return new APIResult()
                {
                    status = ResponseStatusResult.OK
                };
            }
            else
            {
                return new APIResult()
                {
                    status = ResponseStatusResult.BadRequest
                };
            }
        }
    }
}