﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using WSE.IdentityAuth.Model;
using WSE.IdentityAuth.Model.Request;

namespace WSE.IdentityAuth.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientController : ControllerBase
    {
        private readonly ApiAppSetting _appSetting;
        private readonly APITokenOptions _apitokenSetting;

        public ClientController(IOptions<APITokenOptions> apitokenSetting, IOptions<ApiAppSetting> appSetting)
        {
            _appSetting = appSetting.Value;
            _apitokenSetting = apitokenSetting.Value;
        }

        [AllowAnonymous]
        [HttpGet("gettoken")]
        public GetTokenResponse Get([FromQuery]GetTokenRequest request)
        {
            var client = _apitokenSetting.Tokens.FirstOrDefault(p => p.ClientId == request.ClientId && p.ClientSecret == request.ClientSecret);
            if (client == null) return new GetTokenResponse()
            {
                Desc = "UnKnow Client"
            };
            var claims = new[]{
                    new Claim(JwtRegisteredClaimNames.Nbf,$"{new DateTimeOffset(DateTime.Now).ToUnixTimeSeconds()}") ,
                    new Claim (JwtRegisteredClaimNames.Exp,$"{new DateTimeOffset(DateTime.Now.AddMinutes(_appSetting.JWTExpireMinites)).ToUnixTimeSeconds()}"),
                    new Claim("AppName", client.AppName.ToString()),
                    new Claim("Token",client.Token.ToString())
                };
            //sign the token using a secret key.This secret will be shared between your API and anything that needs to check that the token is legit.
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_appSetting.JWTSecurityKey));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            //.NET Core’s JwtSecurityToken class takes on the heavy lifting and actually creates the token.
            var token = new JwtSecurityToken(
                //颁发者
                issuer: _appSetting.JWTIssuer,
                //接收者
                audience: _appSetting.JWTAudience,
                //过期时间
                expires: DateTime.Now.AddMinutes(_appSetting.JWTExpireMinites),
                //签名证书
                signingCredentials: creds,
                //自定义参数
                claims: claims
                );
            return new GetTokenResponse()
            {
                Result = true,
                Token = new JwtSecurityTokenHandler().WriteToken(token)
            };
        }
    }
}