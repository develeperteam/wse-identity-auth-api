﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSE.IdentityAuth.Model;

namespace WSE.IdentityAuth.API
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate next;

        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context, ILogger<ErrorHandlingMiddleware> logger)
        {
            try
            {
                await next(context);
            }
            catch (Exception ex)
            {
                if(ex is APIException)
                {
                    var error = ex as APIException;
                    logger.LogError(ex, GetRequestInfo(context));
                    await HandleExceptionAsync(context, new APIResult
                    {
                        status = new ResponseStatus
                        {
                            statusCode = error.GetErrorCode(),
                            message=error.GetErrorMessage()
                        }
                    }, 200);
                }
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, object data, int statusCode = 406)
        {
            context.Response.ContentType = "application/json;charset=utf-8";
            context.Response.StatusCode = statusCode;
            return context.Response.WriteAsync(JsonConvert.SerializeObject(data));
        }

        private static string GetRequestInfo(HttpContext context)
        {
            StringBuilder message = new StringBuilder();
            message.Append("\r\n************************************\r\n");
            message.Append("Date：" + DateTime.Now.ToString() + "\r\n");
            message.Append("User：" + context.User.Identity.Name + "\r\n");
            message.Append("IP：" + context.Request.Host + "\r\n");
            message.Append("Browser：" + context.Request.Headers[HeaderNames.UserAgent].ToString() + "\r\n");
            message.Append("\r\n************************************\r\n");
            if (context.Request != null &&
            context.Request.HasFormContentType &&
            context.Request.Form != null &&
            context.Request.Form.Count > 0 &&
            context.Request.Form.Keys.Count > 0)
            {
                message.Append("Form Collection：" + "\r\n");
                foreach (object v in context.Request.Form.Keys)
                {
                    if (v == null)
                    {
                        continue;
                    }
                    if (v.ToString()[0] == '_')
                    {
                        continue;
                    }
                    message.Append("Form_key:" + v.ToString() + "   value: " + context.Request.Form[v.ToString()] + "\r\n"); ;
                }
                message.Append("\r\n");
            }
            message.Append("ReferrerUrl:  ");
            message.Append(context.Request.Path == null ? string.Empty : context.Request.Path + "\r\n");
            message.Append("\r\n************************************\r\n");
            return message.ToString();
        }
    }

    public static class ErrorHandlingExtensions
    {
        public static IApplicationBuilder UseErrorHandling(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ErrorHandlingMiddleware>();
        }
    }
}
