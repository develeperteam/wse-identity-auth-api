﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using WSE.IdentityAuth.Interface;
using WSE.IdentityAuth.Model;
using WSE.IdentityAuth.Service;

namespace WSE.IdentityAuth.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var appSetting = new ApiAppSetting();
            Configuration.Bind("appSettings", appSetting);
         
            services.AddOptions();
            services.Configure<ApiAppSetting>(Configuration.GetSection("appSettings"));
            services.Configure<TencentAPIConfig>(Configuration.GetSection("TencentAPIConfig"));
            services.Configure<APITokenOptions>(Configuration.GetSection("APIToken"));
            services.AddScoped<IAuthVerification, TencentAuthService>();


            #region disabled the JToken
            //services.AddAuthentication(option =>
            //{
            //    //添加JWT Scheme
            //    option.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            //    option.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            //    option.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            //}).AddJwtBearer(option =>
            //{
            //    //添加JWT验证
            //    option.TokenValidationParameters = new TokenValidationParameters()
            //    {
            //        ValidateLifetime = true,//是否验证失效时间
            //        ClockSkew = TimeSpan.FromSeconds(appSetting.JWTExpireMinites),
            //        ValidateAudience = true,//是否验证Audience
            //        ValidAudience = appSetting.JWTAudience,//Audience
            //        //这里采用动态验证的方式，在重新登陆时，刷新token，旧token就强制失效了
            //        // AudienceValidator = (m, n, z) =>
            //        // {
            //        //     return m != null && m.FirstOrDefault().Equals(appSetting.JWTAudience);
            //        // },
            //        ValidateIssuer = true,//是否验证Issuer
            //        ValidIssuer = appSetting.JWTIssuer,//Issuer，这两项和前面签发jwt的设置一致
            //        ValidateIssuerSigningKey = true,//是否验证SecurityKey
            //        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(appSetting.JWTSecurityKey))//拿到SecurityKey
            //    };
            //    option.Events = new JwtBearerEvents()
            //    {
            //        OnAuthenticationFailed = context =>
            //        {
            //            context.Response.Headers.Add("ErrorMessage˝", context.Exception.Message);
            //            //Token expired
            //            if (context.Exception.GetType() == typeof(SecurityTokenExpiredException))
            //            {
            //                context.Response.Headers.Add("Token-Expired", "true");
            //                context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
            //                var data = new APIResult()
            //                {
            //                    status = new ResponseStatus
            //                    {
            //                        statusCode = HttpStatusCode.Unauthorized.ToString(),
            //                    }
            //                };
            //                context.Response.WriteAsync(JsonConvert.SerializeObject(data));
            //            }
            //            return Task.CompletedTask;
            //        }
            //    };
            //});

            #endregion

            services.AddMvc();
            services.Configure<ApiBehaviorOptions>(options =>
            {
                //options.SuppressModelStateInvalidFilter = true;
                options.InvalidModelStateResponseFactory = (context) =>
                {
                    var state = context.ModelState.Where(p => p.Value.Errors.Any()).First();
                    string error = state.Value.Errors.First().ErrorMessage;

                    return new JsonResult(new APIResult()
                    {
                        status = new ResponseStatus()
                        {
                            statusCode = "400",
                            message = error
                        }
                    });
                };
            });         

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseErrorHandling();

            //app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
