﻿using System;
using System.Threading.Tasks;
using WSE.IdentityAuth.Model.Request;

namespace WSE.IdentityAuth.Interface
{
    public interface IAuthVerification
    {
        Task<bool> ImageVerification(ImageAuthRequest request);
        Task<bool> PhoneVerification(PhoneAuthRequest request);
        Task<bool> BankCardVerification(BankCardAuthRequest request);

    }
}
