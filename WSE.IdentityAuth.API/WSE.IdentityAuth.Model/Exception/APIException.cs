﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WSE.IdentityAuth.Model
{
    public class APIException : ApplicationException
    {
        private string errorcode;
        private string error;
        private string response;
        private Exception innerException;
        //无参数构造函数
        public APIException()
        {

        }
        //带一个字符串参数的构造函数，作用：当程序员用Exception类获取异常信息而非 MyException时把自定义异常信息传递过去
        public APIException(string errorcode, string msg, string response = "") : base(msg)
        {
            this.errorcode = errorcode;
            this.error = msg;
            this.response = response;
        }
        //带有一个字符串参数和一个内部异常信息参数的构造函数
        public APIException(string errorcode, string msg, Exception innerException, string response = "") : base(msg)
        {
            this.errorcode = errorcode;
            this.innerException = innerException;
            this.error = msg;
            this.response = response;
        }
        
        public string GetErrorCode()
        {
            return errorcode;
        }

        public string GetErrorMessage()
        {
            return error;
        }

        public string GetError()
        {
            return @"Error Code:{error}, Error Message:{error}, Response:{response}";
        }
    }
}  
    
