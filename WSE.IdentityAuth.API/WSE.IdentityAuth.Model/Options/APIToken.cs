﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WSE.IdentityAuth.Model
{
    public class APITokenOptions
    {
        public TokenOpt[] Tokens { get; set; }
    }

    public class TokenOpt
    {
        public string AppName { get; set; }
        public string Token { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
    }
}
