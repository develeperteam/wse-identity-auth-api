﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WSE.IdentityAuth.Model
{
    public class TencentAPIConfig
    {
        public float ImageSim { get; set; }
        public string SecretId { get; set; }
        public string SecretKey { get; set; }
        public string Endpoint { get; set; }
        public string Version { get; set; }
        public string Region { get; set; }
    }
}
