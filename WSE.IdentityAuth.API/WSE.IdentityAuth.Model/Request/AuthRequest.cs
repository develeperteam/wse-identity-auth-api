﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace WSE.IdentityAuth.Model.Request
{
    public class AuthRequest
    {
        [Required(ErrorMessage ="Name required")]
        public string Name { get; set; }
        [Required(ErrorMessage ="ID number required")]
        [RegularExpression(@"^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}(\d|x|X)$", ErrorMessage = "Invaild ID number")]
        public string IDNumber { get; set; }
    }
}
