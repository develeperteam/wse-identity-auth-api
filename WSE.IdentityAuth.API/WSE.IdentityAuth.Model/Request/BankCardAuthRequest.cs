﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace WSE.IdentityAuth.Model.Request
{
    public class BankCardAuthRequest:AuthRequest
    {
        [Required(ErrorMessage ="Band card required")]
        [RegularExpression(@"^(\d{16}|\d{19}|\d{17})$", ErrorMessage = "Invaild Mobile")]
        public string BankAccount { get; set;}
    }
}
