﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WSE.IdentityAuth.Model.Request
{
    public class GetTokenRequest
    {
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
    }
}
