﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace WSE.IdentityAuth.Model.Request
{
    public class ImageAuthRequest : AuthRequest
    {
        public string ImageBaseString { get; set; }
    }
}
