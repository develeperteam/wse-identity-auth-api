﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace WSE.IdentityAuth.Model.Request
{
    public class PhoneAuthRequest : AuthRequest
    { 
        [Required]
        [RegularExpression(@"^1[3|4|5|6|7|8|9]\d{9}$", ErrorMessage = "Invaild Mobile")]
        public string Mobile { get; set; }
    }
}
