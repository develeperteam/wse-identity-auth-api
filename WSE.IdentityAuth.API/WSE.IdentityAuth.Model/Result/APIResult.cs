﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WSE.IdentityAuth.Model
{
    public class APIResult
    {
        public ResponseStatus status { get; set; }
        public object obj { get; set; }
    }

    public class ResponseStatus
    {
        public string statusCode { get; set; }
        public string message { get; set; }
    }

    public class ResponseStatusResult
    {
        public static ResponseStatus OK = new ResponseStatus { statusCode = "200", message = "OK" };
        public static ResponseStatus BadRequest = new ResponseStatus { statusCode = "400", message = "Bad Request" };
    }
}
