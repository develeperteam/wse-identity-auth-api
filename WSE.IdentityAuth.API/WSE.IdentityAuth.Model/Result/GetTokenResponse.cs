﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WSE.IdentityAuth.Model
{
    public class GetTokenResponse
    {
        public bool Result { get; set; }
        public string Desc { get; set; }
        public string Token { get; set; }
    }
}
