﻿using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TencentCloud.Common;
using TencentCloud.Common.Profile;
using TencentCloud.Faceid.V20180301;
using TencentCloud.Faceid.V20180301.Models;
using WSE.IdentityAuth.Interface;
using WSE.IdentityAuth.Model;
using WSE.IdentityAuth.Model.Request;

namespace WSE.IdentityAuth.Service
{
    public class TencentAuthService: IAuthVerification
    {
        private readonly TencentAPIConfig opts;

        public TencentAuthService(IOptions<TencentAPIConfig> _opts)
        {
            opts = _opts.Value;
        }

        public async Task<bool> ImageVerification(ImageAuthRequest request)
        {
            FaceidClient client = GetClient();
            ImageRecognitionRequest req = new ImageRecognitionRequest();

            ImageRecognitionRequest param = new ImageRecognitionRequest();    
            param.IdCard = request.IDNumber;
            param.Name = request.Name;
            param.ImageBase64 = Regex.Replace(request.ImageBaseString, @"^data:image\/[a-z]+;base64,", "");

            var response = await client.ImageRecognition(param);

            if (response != null)
            {
                if (response.Result == "Success")
                {
                    if (response.Sim > opts.ImageSim)
                    {

                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    throw new APIException(response.Result, response.Description, JsonConvert.SerializeObject(response));
                }
            }

            throw new APIException("404","Missing response.");

        }

        public async Task<bool> PhoneVerification(PhoneAuthRequest request)
        {
            FaceidClient client = GetClient();
            PhoneVerificationRequest req = new PhoneVerificationRequest();

            PhoneVerificationRequest param = new PhoneVerificationRequest();            
            param.IdCard = request.IDNumber;
            param.Name = request.Name;
            param.Phone = request.Mobile;

            PhoneVerificationResponse response = client.PhoneVerificationSync(param);

            if (response != null)
            {
                if (response.Result == "0")
                {

                    return true;
                }
                else
                {
                    throw new APIException(response.Result, response.Description, JsonConvert.SerializeObject(response));
                }
            }

            throw new APIException("404", "Missing response.");
        }

        public async Task<bool> BankCardVerification(BankCardAuthRequest request)
        {
            FaceidClient client = GetClient();
            BankCardVerificationRequest req = new BankCardVerificationRequest();

            BankCardVerificationRequest param = new BankCardVerificationRequest();        
            param.IdCard = request.IDNumber;
            param.Name = request.Name;
            param.BankCard = request.BankAccount;
            param.CertType = 0;            
          
            BankCardVerificationResponse response = client.BankCardVerificationSync(param);

            if (response != null)
            {
                if (response.Result == "0")
                {

                    return true;
                }
                else
                {
                    throw new APIException(response.Result, response.Description, JsonConvert.SerializeObject(response));
                }
            }

            throw new APIException("404", "Missing response.");
        }

        private Credential GetCredential()
        {
            return new Credential
            {
                SecretId = opts.SecretId,
                SecretKey = opts.SecretKey
            };
        }

        private FaceidClient GetClient()
        {
            var cred = GetCredential();
            string endpoint = opts.Endpoint;
            ClientProfile clientProfile = new ClientProfile("TC3-HMAC-SHA256");
            HttpProfile httpProfile = new HttpProfile();
            httpProfile.Endpoint = endpoint;
            clientProfile.HttpProfile = httpProfile;

            FaceidClient client = new FaceidClient(cred, opts.Region, clientProfile);
            return client;
        }
    }
}
