import React from 'react';
import {Route } from 'react-router';
import './App.less';
import { PageLayout } from './components/Layout';
import { Mobile } from './components/Mobile';
import { Photo } from './components/Photo';

function App() {
    return (
       <PageLayout>
            <Route exact path={"/"} component={Mobile} />  
            <Route exact path={"/mobile"} component={Mobile} />
            <Route path={"/mobile/:id"} component={Mobile} />
            <Route exact path={"/photo"} component={Photo} />
            <Route path={"/photo/:id"} component={Photo} />
            
      </PageLayout>
   );
}

export default App;
