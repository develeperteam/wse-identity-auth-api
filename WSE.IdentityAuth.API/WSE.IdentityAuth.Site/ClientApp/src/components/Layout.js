import './Layout.less';
import React, { Component } from 'react';
import logo from '../images/logo.svg';


export class PageLayout extends Component {

  render () {
    return (
      <div className="container">
       
        <div className="content">
          <div className="formbox">
             <div className="top">
                <div className="header">
                   <img alt="logo" className="logo" src={logo} />      
                </div>
                <div className="desc"></div>
                {this.props.children}
              </div>
          </div>
        </div>
      </div>
    );
  }
}
