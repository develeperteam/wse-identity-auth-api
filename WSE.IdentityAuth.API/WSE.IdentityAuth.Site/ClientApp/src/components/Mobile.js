import React, { Component } from 'react';
import { Form, Input, Button, message } from 'antd';
import './Mobile.less';
import 'antd/dist/antd.css';

export class Mobile extends Component {

    state = { loading: false, orderno: ""};

    constructor(props) {
        super(props);
    }

    componentWillMount() {
        console.log(this.props.match.params.id);
        this.setState({
            orderno: this.props.match.params.id
        });
        const { orderno } = this.state;
        console.log(orderno);
    }

    async postData(reqData) {
        try {
            this.setState({ loading: true });
            const response = await fetch('auth/bymobile', {
                method: "POST",
                headers: new Headers({
                    "Content-Type": "application/json;charset=UTF-8",
                }),
                body: JSON.stringify(reqData),
            });
            const data = await response.json();
            if (data && data.status) {
                if (data.status.statusCode === "200") {
                    //message.info(getAge(reqData.IDNumber, 3));
                } else {
                    message.error(data.status.message);
                }
            } else {
                message.error('Timeout. Please contact it department');
            }
            this.setState({ loading: false });
        }
        catch (error) {
            message.error('System error. Please contact it department');
            this.setState({ loading: false });
        }
    }

    onFinish = values => {
        console.log('Received values of form: ', values);
        this.postData(values);

    };


    render() {
        const { loading } = this.state;
        const { orderno } = this.state;
        console.log("test:" + orderno);
        return (
            <div className="main"> 
                <div className="title">
                    <h1>信息认证</h1>
                </div>
                <div className="form">
                <Form name="basic" onFinish={this.onFinish}>
                    <Form.Item
                    name="Name"
                    
                    rules={[
                    {
                        required: true,
                            message: '请输入您的姓名',
                    },
                    ]}
                >
                            <Input className="text" placeholder="请输入您的姓名"/>
                </Form.Item>

                <Form.Item
                    name="IDNumber"      
                    
                    rules={[
                    {
                        required: true,
                            message: '请输入您的身份证号码',
                    },
                    {
                        pattern: /^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}(\d|x|X)$/,
                        message: "身份证号格式错误",
                    }
                    ]}
                >
                            <Input className="text" placeholder="请输入您的身份证号码"  />
                </Form.Item>

                 <Form.Item
                        name="Mobile"
                        rules={[
                            {
                                required: true,
                                message: '请输入您的身份证号码',
                            },
                            {
                                pattern: /^1[3|4|5|6|7|8|9]\d{9}$/,
                                message: '手机号格式错误',
                            },
                        ]}
                    >
                            <Input className="text" placeholder="请输入您的手机号"/>
                    </Form.Item>

                    <Form.Item>
                            <Button type="primary" nostyle="true" htmlType="提交" className="submit_btn" loading={loading}>
                        提交
                        </Button>
                    </Form.Item>
                  </Form>
                </div>
                <div className="tips">
                    <div className="tips-left">
                    或者你可以使用？
                    </div>
                    <div className="tips-right">
                        <a className="content" href={orderno ? "/photo/" + orderno : "/photo"}>
                        <div className="btn-red">
                        照片认证                      
                        </div>                      
                    </a>
                    </div>
                </div>
            </div>
        );
      }
}