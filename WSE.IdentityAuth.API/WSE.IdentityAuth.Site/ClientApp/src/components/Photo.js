import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { Form, Input, Button, Upload, message } from 'antd';
import './Photo.less';
import 'antd/dist/antd.css';
import logo from '../images/upload.png';

export class Photo extends Component {
    state = { loading: false, uploading: false, orderno:"" };

    constructor(props) {
        super(props);
        
        
    }

    componentWillMount() {
        console.log(this.props.match.params.id);
        this.setState({
            orderno: this.props.match.params.id
        });
    }

   

    beforeUpload = (file) => {
        const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
        if (!isJpgOrPng) {
            this.setState({ isVaild: false });
            message.error('请上传JPG或PNG格式图片');
            return false;
        }
        else {
            const isLtSize = ((file.size / 1024) / 1024) <= 3;
            if (!isLtSize) {
                this.setState({ isVaild: false });
                message.error('图片请勿超过3M');
                return false;
            } else {
                this.setState({ isVaild: true });
                return isLtSize;
            }
        }
    };

    getBase64 = (img, callback) => {
        const reader = new FileReader();
        reader.addEventListener('load', () => callback(reader.result));
        reader.readAsDataURL(img);
    };

    async postData(reqData) {
        try {
            this.setState({ loading: true });
            const response = await fetch('auth/byimage', {
                method: "POST",
                headers: new Headers({
                    "Content-Type": "application/json;charset=UTF-8",
                }),
                body: JSON.stringify(reqData),
            });
            const data = await response.json();
            if (data && data.status) {
                if (data.status.statusCode === "200") {
                    //message.info(getAge(reqData.IDNumber, 3));
                } else {
                    message.error(data.status.message);
                }
            } else {
                message.error('Timeout. Please contact it department');
            }
            this.setState({ loading: false });
        }
        catch (error) {
            message.error('System error. Please contact it department');
            this.setState({ loading: false });
        }
    }

    handleChange = info => {
        if (this.state.isVaild) {
            this.setState({ uploading: true });
            this.getBase64(info.file.originFileObj, imageUrl =>
                this.setState({
                    imageUrl,
                    uploading: false,
                }),
            );
        }
    };

    render () {
        const uploadButton = (
            <div className="uploadpic">
			    <img src={logo} />
			</div>
        );
        const { loading } = this.state;
        const { imageUrl } = this.state;
        const { orderno } = this.state;
        console.log("test:" + orderno);
        return (
            <div className="main"> 
                <div className="title">
                    <h1>信息认证</h1>
                </div>
                <div className="form">
                  <Form name="basic">
                    <Form.Item
                    name="Name"
                    rules={[
                    {
                        required: true,
                        message: '请输入您的姓名',
                    },
                    ]}
                >
                            <Input placeholder="请输入您的姓名"
                                className="text"/>
                </Form.Item>

                <Form.Item
                    name="IDNumber"
                    rules={[
                    {
                        required: true,
                        message: '请输入您的身份证号码',
                    },
                    {
                        pattern: /^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}(\d|x|X)$/,
                        message: "身份证号格式错误",
                    }
                    ]}
                >
                <Input placeholder="请输入您的身份证号码"
                                className="text" />
                </Form.Item>

                  <Form.Item 
                        name="Upload">
                        <Upload 
                            name="avatar"
                            listType="picture-card"
                            className="avatar-uploader"    
                            showUploadList={false}
                            beforeUpload={this.beforeUpload} 
                            onChange={this.handleChange}
                            >
                                {imageUrl ? <img src={imageUrl} alt="avatar" style={{ height:'120px' }} /> : uploadButton}
                        </Upload>
                 
                    </Form.Item>

                        <Form.Item>
                            <Button type="primary" nostyle htmlType="submit" className="submit_btn" loading={loading}>
                        提交
                        </Button>
                    </Form.Item>
                  </Form>
                </div>
                <div className="tips">
                    <div className="tips-left">
                    或者你可以使用？
                    </div>
                    <div className="tips-right">
                        <a className="content" href={orderno ? "/mobile/" + orderno: "/mobile" } >
                        <div className="btn-red">
                         手机认证
                        </div>
                     </a>
                    </div>
                </div>
            </div>
        );
      }
}