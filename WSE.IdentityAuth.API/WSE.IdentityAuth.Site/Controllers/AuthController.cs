﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WSE.IdentityAuth.Interface;
using WSE.IdentityAuth.Model;
using WSE.IdentityAuth.Model.Request;
using WSE.IdentityAuth.Service;

namespace WSE.IdentityAuth.Site.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthVerification authRepository;

        private readonly AuthService _authService;

        public AuthController(IAuthVerification _authRepository, AuthService authService)
        {
            authRepository = _authRepository;

            _authService = authService;
        }

        [Route("bymobile")]
        public async Task<APIResult> MobileVerification(PhoneAuthRequest request)
        {

            var result = await _authService.MobileVerification(request);
            return result;
        }

        [Route("byimage")]
        public async Task<APIResult> ImageVerification(ImageAuthRequest request)
        {
            var result = await _authService.ImageVerification(request);

            return result;
        }
    }
}